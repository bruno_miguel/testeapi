# Etapas

## Visualização dos teste na pipeline

1. Acesse a opção `Build -> Pipelines` (no lado esquerdo da tela)
2. Clique na pipeline indicada com `latest` (número que começa com `#`)
3. Acesse a opção `Jobs`
4. Terão 2 jobs já rodados (1 com sucesso, 1 com falha)
   1. Clique em cima da opção `passed` para visualizar exemplo de teste com sucesso
   2. Para acessar o job reprovado, altere a opção selecionada (abaxo de "pipeline" no lado direito da tela), após isso clique no botão abaixo
5. É possível baixar os artefatos (relatório dos testes, gerado de forma automática) clicando na opção `Download` à direita da tela
   1. Será baixado uma pasta com 2 arquivos html, um para cada job

## Rodar testes via Docker

1. Clone o projeto do Gitlab
2. Acesse o projeto em sua máquina
3. Abra o Docker Desktop
4. Escreva no terminal `docker build -t teste:latest .`
5. Escreva no terminal `docker run -t teste:latest run testeapi.postman_collection.json --insecure`
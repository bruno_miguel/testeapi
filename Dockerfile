FROM node:14.18-alpine3.12

RUN apk update && apk upgrade

RUN npm config set strict-ssl false

RUN apk add --no-cache musl-dev gcc build-base libc-dev curl git ca-certificates &&  \
    update-ca-certificates

ARG cert_location=/usr/local/share/ca-certificates
RUN apk add ca-certificates openssl
RUN openssl s_client -showcerts -connect registry.npmjs.org:443 </dev/null 2>/dev/null|openssl x509 -outform PEM >  ${cert_location}/newman.crt
RUN cat /usr/local/share/ca-certificates/newman.crt >> /etc/ssl/certs/ca-certificates.crt
RUN update-ca-certificates

RUN npm install -g newman 
RUN npm install -g newman-reporter-html

COPY . .

ENTRYPOINT [ "newman" ]
